# burp

A small shell script used to literally burp a piece of text to stdout.

Usage: burp [count] [msg]

count and msg default to 1 and stdin respectively.